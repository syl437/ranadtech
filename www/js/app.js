// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services',
    'ngStorage', 'xml', 'ngFileUpload', 'angularMoment', 'ui.bootstrap', 'ui.bootstrap.datetimepicker'])

    .run(function ($http, $ionicHistory, $localStorage, $ionicPlatform, $ionicSideMenuDelegate, $rootScope, $state, AuthService) {
        $ionicPlatform.ready(function () {
            if (window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }
            $http.get('https://tapper.co.il/elalranad/info.php').then(function(data){$rootScope.info = data.data;});
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });

        $rootScope.firstName = $localStorage.firstName;
        $rootScope.lastName = $localStorage.lastName;
        $rootScope.link = $localStorage.link;

        if ('addEventListener' in document) {
            document.addEventListener('DOMContentLoaded', function() {
                FastClick.attach(document.body);
            }, false);
        }

        $rootScope.toggleRightSideMenu = function() {

            $ionicSideMenuDelegate.toggleRight();

        };

        $rootScope.goBack = function() {

            $ionicHistory.goBack();

        };

        // $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        //
        //     if (toState.name != "app.login" && !AuthService.isAuthenticated()) {
        //
        //         event.preventDefault();
        //
        //         window.location ="#/app/login";
        //         // $state.go('app.login');
        //
        //     }
        //
        // });
        //
        // $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        //
        //     if (typeof $rootScope.info !== 'undefined' && $rootScope.info != '1'){
        //
        //         event.preventDefault();
        //
        //     }
        //
        //     if (toState.name == "app.login" && AuthService.isAuthenticated()) {
        //
        //         event.preventDefault();
        //
        //         if (AuthService.isManager())
        //             window.location ="#/app/manager";
        //             // $state.go('app.manager');
        //         else
        //             window.location ="#/app/attendance";
        //             // $state.go('app.attendance');
        //
        //     }
        //
        // });
    })

    .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })

            .state('app.login', {
                url: '/login',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/login.html',
                        controller: 'LoginCtrl'
                    }
                }

            })

            .state('app.worker', {
                url: '/worker/:id',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/worker.html',
                        controller: 'WorkerCtrl'
                    }
                }

            })

            .state('app.attendance', {
                url: '/attendance',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/attendance.html',
                        controller: 'AttendanceCtrl'
                    }
                }

            })

            .state('app.manager', {
                url: '/manager',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/manager.html',
                        controller: 'ManagerCtrl'
                    }
                }

            })

            .state('app.add', {
                url: '/add',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/add.html',
                        controller: 'AddCtrl'
                    }
                }

            })


            .state('app.statistics', {
                url: '/statistics/:id',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/statistics.html',
                        controller: 'StatisticsCtrl'
                    }
                }

            })
        ;
        // if none of the above states are matched, use this as the fallback router
        $urlRouterProvider.otherwise('/app/login');

        $httpProvider.interceptors.push('ElAlWebServiceHttpInterceptor');
    });