angular.module('starter.services', [])

    .constant('ELAL_START_URL', 'http://62.219.199.131/CustomerMobileWS/Service1.asmx/GetServiceByID')

    // .constant('ELAL_URL', 'https://Isufit-01/ElAlES/service1.asmx')
    // .constant('ELAL_URL', 'http://213.57.111.137/IsufitWS/Service1.asmx/')

    .service('AuthService', AuthService)

    .service('ElAlApiService', ElAlApiService)

    .factory('ElAlWebService', ElAlWebService)

    .factory('ElAlWebServiceHttpInterceptor', ElAlWebServiceHttpInterceptor);


function AuthService($http, $rootScope, $localStorage, $q) {

    // Public

    this.authenticate = authenticate;

    this.getUser = getUser;

    this.isAuthenticated = isAuthenticated;

    this.isManager = isManager;

    this.logout = logout;

    // Private

    function authenticate(username, password) {

        var deferred = $q.defer();

        $http.post($localStorage.link + 'CheckWorkerLogin', {
            userID: username,
            password: password
        }).then(success, failure);

        function success(response) {

            // If credentials are invalid

            if (response.UseID == "0") {

                deferred.reject({error: 'invalidCredentials'});

                return;
            }

            // Else

            $localStorage.userId = response.UseID;
            $localStorage.isManager = parseInt(response.IsManager) == 1;
            $localStorage.permission = response.Harshaa;
            $localStorage.firstName = response.FirstName;
            $localStorage.lastName = response.familyName;

            $rootScope.firstName = $localStorage.firstName;
            $rootScope.lastName = $localStorage.lastName;

            deferred.resolve();

        }

        function failure(error) {

            deferred.reject();

        }

        return deferred.promise;

    }

    function logout() {

        delete $localStorage.userId;
        delete $localStorage.isManager;
        delete $localStorage.permission;
        delete $localStorage.firstName;
        delete $localStorage.lastName;

    }

    function isAuthenticated() {
        return $localStorage.userId !== undefined;
    }

    function isManager() {
        return $localStorage.isManager;
    }

    function getUser() {
        return $localStorage.userId;
    }

}

function ElAlApiService($http, $q, $localStorage, moment) {

    this.getCurMonthAttendance = getCurMonthAttendance;

    this.getSmalim = getSmalim;

    this.formatDuration = formatDuration;

    function getCurMonthAttendance(userId, from, to) {

        var deferred = $q.defer();

        $http.post($localStorage.link + 'NewGetWorkingAttendance', {

            "userid": userId,
            "attendanceFromDate": from.format('DD/MM/YYYY'),
            "attendanceToDate": to.format('DD/MM/YYYY')

        }).then(success, failure);

        function success(response) {

            // Total duration

            var totalDuration = moment.duration();

            // Iterate over received array

            for (var i = 0; i < response.length; i++) {

                // Set unique id

                response[i].id = i;

                // Parse date from API

                response[i].Cur_Date = moment(response[i].Cur_Date, 'DD/MM/YYYY H:m:s');

                // Handle IN122 and OUT122 only if they both are not equal ''

                if (response[i].IN122 !== "" && response[i].OUT122 !== "") {

                    totalDuration.add(response[i].SAC122);

                }
            }

            // Pass data to view

            deferred.resolve({

                curMonthAttendance : angular.copy(response),
                totalTime : formatDuration(totalDuration)

            });

        }

        function failure(errors) {

            deferred.reject();

        }

        return deferred.promise;
    }

    // Formats duration as H.mm

    function formatDuration(duration) {

        var days = duration.days();
        var hours = duration.hours();
        var minutes = duration.minutes();

        return (days * 24 + hours) + '.' + (minutes < 10 ? '0' + minutes : minutes);

    }

    // get categories

    function getSmalim(userId) {

        var deferred = $q.defer();

        $http.post($localStorage.link + 'GetSmalim', {"OvedId": userId}).then(successSmalim, failure);

        function successSmalim(response) {

            var data = response;
            data.unshift({ID: '0', Name: 'בחר קטגוריה'});
            return deferred.resolve({data : data});

        }

        function failure(errors) {

            deferred.reject();

        }

        return deferred.promise;

    }


}

function ElAlWebService(ELAL_START_URL, $localStorage) {

    return {

        AGREEMENTS: $localStorage.link + 'GetAgreements',
        ATTENDANCE: $localStorage.link + 'NewGetWorkingAttendance',
        AUTHENTICATE: $localStorage.link + 'CheckWorkerLogin',
        CHECKOUT: $localStorage.link + 'SaveUserAttendanceWS',
        DEPARTMENTS: $localStorage.link + 'GetMahlakotWS',
        DIVISIONS: $localStorage.link + 'GetDivisionsWS',
        FACTORIES: $localStorage.link + 'GetFactoriesWS',
        LINK: ELAL_START_URL,
        POSITIONS: $localStorage.link + 'GetPositions',
        UPLOAD: $localStorage.link + 'WorkerDocumentsSave',
        SEARCH: $localStorage.link + 'GetWorkers',
        SIGN: $localStorage.link + 'SaveDigitalSign',
        SMALIM: $localStorage.link + 'GetSmalim',
        STATISTICS: $localStorage.link + 'GetReportsWS'

    }

}


function ElAlWebServiceHttpInterceptor($localStorage, ELAL_START_URL, $httpParamSerializer, x2js) {

    return {

        'request': function (config) {

            // For regular requests do nothing

            if (!config.url.startsWith($localStorage.link) && !config.url.startsWith(ELAL_START_URL))
                return config;

            if (config.url.startsWith($localStorage.link + 'WorkerDocumentsSave'))
                return config;

            // For El Al API requests transform it to applicable form

            if(config.url != $localStorage.link + 'WorkerDocumentsSave'){

                config.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
                config.paramSerializer = $httpParamSerializer;
                config.data = $httpParamSerializer(config.data);

            }

            return config;
        },

        'response': function (data) {

            // For regular requests do nothing

            if (!data.config.url.startsWith($localStorage.link) && !data.config.url.startsWith(ELAL_START_URL))
                return data;

            // For El Al API requests transform it to applicable form

            var response = x2js.xml_str2json(data.data);

            if (response.hasOwnProperty('string')) {

                if (data.config.url == $localStorage.link + 'CheckWorkerLogin')
                    return JSON.parse(response.string.toString()).pop();
                else if (data.config.url == ELAL_START_URL)
                    return response.string.toString();
                else
                    return JSON.parse(response.string.toString());

            } else if (response.hasOwnProperty('int')) {

                return parseInt(response.int.toString());

            }

        }

    }

}
