// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'starter.controllers', 'ngStorage', 'xml', 'ngFileUpload'])

    .run(function ($ionicPlatform, $ionicSideMenuDelegate, $rootScope, $ionicHistory) {
        $ionicPlatform.ready(function () {
            if (window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });

        $rootScope.toggleRightSideMenu = function() {

            $ionicSideMenuDelegate.toggleRight();

        };

        $rootScope.goBack = function() {

            $ionicHistory.goBack();

        };
    })


    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })

            .state('app.login', {
                url: '/login',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/login.html',
                        controller: 'LoginCtrl'
                    }
                }

            })

            .state('app.worker', {
                url: '/worker',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/worker.html',
                        controller: 'WorkerCtrl'
                    }
                }

            })

            .state('app.manager', {
                url: '/manager',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/manager.html',
                        controller: 'ManagerCtrl'
                    }
                }

            })

            .state('app.add', {
                url: '/add',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/add.html',
                        controller: 'AddCtrl'
                    }
                }

            })


            .state('app.statistics', {
                url: '/statistics',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/statistics.html',
                        controller: 'StatisticsCtrl'
                    }
                }

            })
        ;
        // if none of the above states are matched, use this as the fallback router
        $urlRouterProvider.otherwise('/app/login');
    });